# Предупреждение
В данном репозитории нет реализованных проектов, только ссылки на проекты и сертификаты.

# Projects

# Stepik
Мой профиль - https://stepik.org/users/61236640

#### Ссылки на пройденные курсы:
1. "Поколение Python": курс для начинающих - https://stepik.org/course/58852/promo
2. "Поколение Python": курс для продвинутых - https://stepik.org/course/68343/promo
3. Программирование на Python - https://stepik.org/course/67/promo
4. Интерактивный тренажер по SQL - https://stepik.org/course/63054/promo
5. Go (Golang) - первое знакомство - https://stepik.org/course/100208/promo
6. Программирование на Golang - https://stepik.org/course/54403/promo 
